package fiuba.algo3.tablero;

import org.junit.Assert;
import org.junit.Test;

public class posicionTest {

	@Test
	public void testCreoDosPosicionesIgualesLaComparacionDebeDarTrue(){
		Posicion posicion1 = new Posicion(3,5);
		Posicion posicion2 = new Posicion(3,5);
		Assert.assertTrue(posicion1.equals(posicion2));
	}

	@Test
	public void testCreoDosPosicionesDistintasLaComparacionDebeDarFalse(){
		Posicion posicion1 = new Posicion(3,5);
		Posicion posicion2 = new Posicion(3,4);
		Assert.assertFalse(posicion1.equals(posicion2));	
	}
	
	@Test (expected = PosicionInvalidaException.class)
	public void testCreoPosicionConValorNegativoEnXDebeLanzarExcepcion(){
		new Posicion(-1,3);
	}
	
	@Test (expected = PosicionInvalidaException.class)
	public void testCreoPosicionConValorNegativoEnYDebeLanzarExcepcion(){
		new Posicion(5,-2);
	}
}
