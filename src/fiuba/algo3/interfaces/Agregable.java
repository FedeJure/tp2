package fiuba.algo3.interfaces;

import fiuba.algo3.tablero.Casillero;

public interface Agregable {

	void ubicarEn(Casillero casillero);
	
}
