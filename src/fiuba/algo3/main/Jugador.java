package fiuba.algo3.main;

import fiuba.algo3.algoformer.Algoformer;
import fiuba.algo3.tablero.Tablero;

public class Jugador {

	private boolean enJuego;
	private final int equipo;
	
	private Algoformer[] algoformers;
	
	public Jugador(int equipo){
		enJuego = true;
		this.equipo = equipo;
	}

	public void posicionarPersonajes(Tablero tablero) {
		tablero.inicializarAlgoformers(algoformers,equipo);
		
	}
}
