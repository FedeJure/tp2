package fiuba.algo3.tablero;

import fiuba.algo3.algoformer.Algoformer;
import fiuba.algo3.interfaces.Agregable;
import fiuba.algo3.tablero.direccion.Direccion;

public class Casillero {

	private Posicion posicion;
	private Tablero tablero;
	private Agregable contenido; 
	//mas adelante seguramente sea un "Colocable" asi puede ser algoformer, bonus o la chispa suprema.
	
	public Casillero(Tablero tablero, Posicion posicion) {
		this.tablero = tablero;
		this.posicion = posicion;
		contenido = null;

	}

	public void colocar(Agregable algoformer) {
		this.contenido = algoformer;
		algoformer.ubicarEn(this);

	}

	public Posicion getPosicion() {
		return posicion;
	}

	public void colocarEnSiguiente(Algoformer algoformer, Direccion direccion) {
		int siguientePosicionEnX = this.posicion.getPosicionX() + direccion.getDireccionX();
 		int siguientePosicionEnY = this.posicion.getPosicionY() + direccion.getDireccionY();
		Posicion siguientePosicion = new Posicion(siguientePosicionEnX, siguientePosicionEnY);
		
		this.tablero.agregar(algoformer, siguientePosicion);
		liberarCasillero();
		
		
	}
	private void liberarCasillero(){
		contenido = null;
	}

	public boolean estaLibre() {
		return (contenido == null);
	}

	public void lanzarAtaque(Algoformer objetivo, Algoformer atacante) {
		tablero.dirigirAtaque(objetivo,atacante);
		
	}

}
