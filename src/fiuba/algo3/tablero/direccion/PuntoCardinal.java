package fiuba.algo3.tablero.direccion;

public enum PuntoCardinal {
	CENTRO,NORTE,SUR,ESTE,OESTE,NORESTE,NOROESTE,SURESTE,SUROESTE
}
