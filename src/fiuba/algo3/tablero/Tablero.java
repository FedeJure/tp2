package fiuba.algo3.tablero;

import java.util.HashMap;

import fiuba.algo3.algoformer.Algoformer;
import fiuba.algo3.interfaces.Agregable;
import fiuba.algo3.tablero.direccion.Direccion;
import fiuba.algo3.tablero.direccion.Este;
import fiuba.algo3.tablero.direccion.Noreste;
import fiuba.algo3.tablero.direccion.Noroeste;
import fiuba.algo3.tablero.direccion.Norte;
import fiuba.algo3.tablero.direccion.Oeste;
import fiuba.algo3.tablero.direccion.PuntoCardinal;
import fiuba.algo3.tablero.direccion.Sur;
import fiuba.algo3.tablero.direccion.Sureste;
import fiuba.algo3.tablero.direccion.Suroeste;

public class Tablero {

	private static final int DIMENSION = 20;
	private HashMap<Posicion,Casillero> casilleros;
	
	public Tablero(){
		casilleros = new HashMap<Posicion,Casillero>();
		Posicion posicion;
		Casillero casillero;
		
		for (int i = 0; i<DIMENSION; i++){
			for (int j = 0; j<DIMENSION; j++){
				posicion = new Posicion(i,j);
				casillero = new Casillero(this,posicion);
				casilleros.put(posicion,casillero);
			}
		}	
	}
	
	public int getDimension(){
		return DIMENSION;		
	}
	public boolean estaLibreEn(Posicion posicion){
		return casilleros.get(posicion).estaLibre();
	}

	//luego puede ser solo agregar(Colocable,posicion);
	public void agregar(Agregable algoformer, Posicion posicion) {
		Casillero unCasillero = casilleros.get(posicion);
		unCasillero.colocar(algoformer);
		casilleros.put(posicion, unCasillero);	
	}

	public void inicializarAlgoformers(Algoformer[] algoformers, int equipo) {
		int corrimiento=0;
		for (Algoformer algoformer : algoformers){
			agregarAPuntoCardinal(algoformer,PuntoCardinal.values()[equipo],corrimiento);
			corrimiento = recalcularCorrimiento(corrimiento);
			
		}
	}

	public void agregarAPuntoCardinal(Agregable agregable, PuntoCardinal puntoCardinal, int corrimiento) {
		switch (puntoCardinal){
		case CENTRO: agregar(agregable,new Posicion(DIMENSION/2,DIMENSION/2));
		case NORTE: agregar(agregable,new Posicion(DIMENSION/2 + corrimiento,0));
		case SUR: agregar(agregable,new Posicion(DIMENSION/2 + corrimiento,DIMENSION-1));
		case ESTE: agregar(agregable,new Posicion(DIMENSION-1,DIMENSION/2 + corrimiento));
		case OESTE: agregar(agregable,new Posicion(0,DIMENSION/2 + corrimiento));
		case NORESTE: agregar(agregable,new Posicion((DIMENSION-1 + corrimiento)>(DIMENSION-1) ? DIMENSION-1 : 
							DIMENSION-1 + corrimiento,(0 + corrimiento) < 0 ? 0 : 0 + corrimiento));
		
		case NOROESTE: agregar(agregable, new Posicion((0 -corrimiento) < 0 ? 0 : 0 -corrimiento
								,(0 + corrimiento) < 0 ? 0 : 0 + corrimiento));
		
		case SURESTE: agregar(agregable, new Posicion( (DIMENSION-1 + corrimiento) > (DIMENSION-1) ?
						DIMENSION-1 : DIMENSION-1 + corrimiento,(DIMENSION-1 -corrimiento) > (DIMENSION-1) ?
						DIMENSION-1 : DIMENSION-1 -corrimiento));
		case SUROESTE: agregar(agregable, new Posicion((0 + corrimiento) < 0 ? 0 : 0 + corrimiento
				,(DIMENSION-1 - corrimiento) > DIMENSION-1 ? DIMENSION-1 : DIMENSION-1- corrimiento));

		}
		
	}
	private int recalcularCorrimiento(int corrimiento){
		if (corrimiento == 0) return 1;
		if (corrimiento > 0) return -corrimiento;
		return (-corrimiento +1);
		
	}



	public void dirigirAtaque(Algoformer objetivo, Algoformer atacante) {
		if (alcanzaDistanciaAtaque(atacante,objetivo)){
			objetivo.recibirAtaque(atacante.getAtaque());
		}
		
	}

	private boolean alcanzaDistanciaAtaque(Algoformer atacante, Algoformer objetivo) {
		int distMax = atacante.getDistanciaAtaque();
		Posicion origen = atacante.getPosicion();
		Posicion destino = objetivo.getPosicion();
		if ((Math.abs(origen.getPosicionX() - destino.getPosicionX()) <= distMax) && 
				(Math.abs(origen.getPosicionY() - destino.getPosicionY()) <= distMax)){
			return true;
		}
			
		return false;
	}

}
