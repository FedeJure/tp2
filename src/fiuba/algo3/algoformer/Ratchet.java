package fiuba.algo3.algoformer;

public class Ratchet extends Algoformer{

	public Ratchet() {
		super(150);
		modoHumanoide = new ModoHumanoide(5,5,1);
		modoAlterno = new ModoAlterno(35,2,8);
		modoActual = modoHumanoide;
	}

}
