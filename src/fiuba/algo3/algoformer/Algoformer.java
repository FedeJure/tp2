package fiuba.algo3.algoformer;

import fiuba.algo3.interfaces.Agregable;
import fiuba.algo3.tablero.Casillero;
import fiuba.algo3.tablero.Posicion;
import fiuba.algo3.tablero.Tablero;
import fiuba.algo3.tablero.direccion.Direccion;

public abstract class Algoformer implements Agregable{
	//borrar este comentario!
	private int vida;
	
	protected Modo modoHumanoide;
	protected Modo modoAlterno;
	protected Modo modoActual;
	
	private Casillero casillero;


	public Algoformer(int vida){
		this.vida = vida;
	}
	
	public int getVelocidad() {
		return modoActual.getVelocidad();
	}

	public void mover(Direccion direccion) {
		for (int i=0; i<getVelocidad(); i++){
			casillero.colocarEnSiguiente(this,direccion);
		}
	}

	public void ubicarEn(Casillero casillero) {
		this.casillero = casillero;
	}

	public Posicion getPosicion() {
		return casillero.getPosicion();
	}

	public void transformar() {
		if (modoActual == modoHumanoide) {
			modoActual=modoAlterno;
			return;
		}
		modoActual = modoHumanoide;
		
		
	}

	public String getModo() {
		return modoActual.getModo();
	}
	public int getVida(){
		return vida;
	}
	public void atacar(Algoformer algoformer){
		casillero.lanzarAtaque(algoformer,this);
	}

	public int getDistanciaAtaque() {
		return modoActual.getDistanciaAtaque();
	}

	public int getAtaque() {
		return modoActual.getAtaque();
	}

	public void recibirAtaque(int ataque) {
		vida -= ataque;
		
	}


}
