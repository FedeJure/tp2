package fiuba.algo3.algoformer;

public abstract class Modo {

	private int ataque;
	private int distanciaAtaque;
	private int velocidad;
	
	public Modo(int ataque,int distanciaAtaque,int velocidad){
		this.ataque = ataque;
		this.distanciaAtaque = distanciaAtaque;
		this.velocidad = velocidad;
	}
	public int getVelocidad() {
		return velocidad;
	}
	public abstract String getModo();
	public int getAtaque() {
		return ataque;
	}
	public int getDistanciaAtaque() {
		return distanciaAtaque;
	}

}
