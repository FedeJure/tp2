package fiuba.algo3.algoformer;

public class Megatron extends Algoformer{

	public Megatron() {
		super(550);
		modoHumanoide = new ModoHumanoide(10,3,1);
		modoAlterno = new ModoAlterno(55,2,8);
		modoActual = modoHumanoide;
	}

}
